//
//  FlightRouteModel.swift
//  TripPlanner
//
//  Created by Binoy Jose on 19/07/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import Foundation
import MapKit

// MARK: - Response
struct Response: Decodable {
    let connections: [FligtConnection]
}

// MARK: - Result
struct Result : Decodable {
    let connections: [FligtConnection]
}

// MARK: - LatLong
struct LatLong : Decodable {
    let lat: Double
    let long: Double
}

// MARK: - Coordinates
struct Coordinates: Decodable {
    let from, to: LatLong
}

// MARK: - Coordinates
struct GeoCoordinates: Decodable {
    let lat, long: Double
}

// MARK: - Departure
struct Departure: Decodable {
    var latlong: LatLong
    init(_ latlong: LatLong) {
        self.latlong = latlong
    }
}

// MARK:- Flight
struct Flight: Decodable {
    var arrival: Departure?
    var departure: Departure?
    init(arrival: Departure, departure: Departure) {
        self.arrival = arrival
        self.departure = departure
    }
}

//MARK:- Schedule
struct Schedule: Decodable {
    var flights: [Flight]?
    init() {
        flights = []
    }
}
