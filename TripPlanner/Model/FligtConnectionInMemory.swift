//
//  FligtConnectionInMemory.swift
//  TripPlanner
//
//  Created by Binoy Jose on 21/07/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import Foundation

class FligtConnectionInMemory {
    let id: String
    let holder: FligtConnection?
    init(_ id: String, _ `for`: FligtConnection? = nil, _ swap: Bool = false) {
        self.id = id
        self.holder = swap ? FligtConnection(swap:`for`!) : `for`
    }
}

extension FligtConnectionInMemory: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(description.hashValue)
    }
}

extension FligtConnectionInMemory: Equatable {
    public static func == (lhs: FligtConnectionInMemory, rhs: FligtConnectionInMemory) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}

extension FligtConnectionInMemory: CustomStringConvertible {
    var description: String {
        return id
    }
}
