//
//  FligtConnection.swift
//  TripPlanner
//
//  Created by Binoy Jose on 21/07/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import Foundation

class FligtConnection : Decodable {
    let from: String
    let to: String
    var coordinates: Coordinates
    let price: Double
    init(swap: FligtConnection) {
        to = swap.from
        from = swap.to
        coordinates = Coordinates(from: swap.coordinates.to,
                                  to: swap.coordinates.from)
        price = swap.price
    }
}

extension FligtConnection: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(description.hashValue)
    }
}

extension FligtConnection: Equatable {
    public static func == (lhs: FligtConnection, rhs: FligtConnection) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}

extension FligtConnection: CustomStringConvertible {
    var description: String {
        return "\(from)|\(to)"
    }
}

