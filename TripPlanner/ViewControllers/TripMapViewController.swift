//
//  TripMapViewController.swift
//  TripPlanner
//
//  Created by Binoy Jose on 19/07/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import UIKit
import MapKit

class TripMapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet private var mapView: MKMapView!
    
    var connectionArray = Array<connectionDetails>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createSelectedRouteInMap()
    }
    
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- drawing route on the map
    
    func createSelectedRouteInMap() {
        if connectionArray.count > 0 {
            var coordinates = Array<CLLocationCoordinate2D>()
            for route in connectionArray {
                let routeCount = route.paths.count
                for i in 0 ... routeCount-1 {
                    var destination = ""
                    
                    // Checking is that the last element of the destination and source is same
                    if routeCount-1 == i {
                        destination = route.paths[i]
                    } else {
                        destination = route.paths[i+1]
                    }
                    
                    if let sorceCoordinates:GeoCoordinates = suggestedCountries[route.paths[i]],
                        let destinationCoordinates:GeoCoordinates = suggestedCountries[destination] {
                        setRegionAndAddAnnotation(latitude: sorceCoordinates.lat,
                                                  longitude: sorceCoordinates.long,
                                                  pinTitle: route.paths[i],
                                                  coordinates: &coordinates)
                        
                        // Setting Region And Add Annotation for destination
                        setRegionAndAddAnnotation(latitude: destinationCoordinates.lat,
                                                  longitude: destinationCoordinates.long,
                                                  pinTitle: destination,
                                                  coordinates: &coordinates)
                    }
                }
                
                //Creating polyline between the coordinates
                let polyline = MKPolyline(coordinates: coordinates, count: coordinates.count)
                mapView.addOverlay(polyline)
            }
        }
    }
    
    // Setting Region And Add Annotation in the mapView
    func setRegionAndAddAnnotation(latitude:Double, longitude:Double, pinTitle:String, coordinates:inout Array<CLLocationCoordinate2D>) {
        let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let span = MKCoordinateSpan(latitudeDelta: 150, longitudeDelta: 150)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        mapView.setRegion(region, animated: true)
        
        let pin = MKPointAnnotation()
        pin.coordinate = coordinate
        pin.title = pinTitle
        mapView.addAnnotation(pin)
        coordinates.append(coordinate)
    }
    
    //MARK:- MKMapViewDelegate
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let testlineRenderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        testlineRenderer.strokeColor = .blue
        testlineRenderer.lineWidth = 2.0
        return testlineRenderer
    }
}
