//
//  MainViewController.swift
//  TripPlanner
//
//  Created by Binoy Jose on 19/07/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import UIKit

struct connectionDetails {
    let paths:Array<String>
    let totalAmout:Double
}

class MainViewController: UIViewController, UITextFieldDelegate {
    
    var cheapestPath = Array<FlightGraphEdge<String>>()
    var connectionArray = Array<connectionDetails>()
    let jsonHandler = JSONHandler()
    let cheapestFlightFinder = CheapestFlightFinder()
    
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var toTextField: UITextField!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var mapRouteButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addTargets()
        
        // Fetching data from JSON in background
        jsonHandler.fetchDataFromJSONinBackground()
    }
    
    func addTargets() {
        fromTextField.addTarget(self, action: #selector(MainViewController.textFieldDidChangeSelection(_:)), for: .editingChanged)
        toTextField.addTarget(self, action: #selector(MainViewController.textFieldDidChangeSelection(_:)), for: .editingChanged)
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        totalPrice.text = updatePrice()
    }
    
    @IBAction func searchButton(_ sender: Any) {
        totalPrice.text = updatePrice()
    }
    
    func updatePrice() -> String {
        // Finding the cheapest price from source to destination (directional search) use edgeType as .directed
        // If we need price(weight) available to both direction (unidirectional search) use edgeType as .undirected
        let price = cheapestFlightFinder.findCheapestFlight(edgeType: .directed,
                                                            source: removeSpacesFromFrontAndBack(stringValue: fromTextField.text!),
                                                            dest: removeSpacesFromFrontAndBack(stringValue: toTextField.text!),
                                                            data: dataFromJSON,
                                                            connectionArray:&connectionArray)
        
        updateNotificationLabelWithConnection(connections: (connectionArray.first?.paths.count) ?? 0)
        return "Total: \(price.total)"
    }
    
    // This function helps to remove the whitespaces from the beginning and end o
    func removeSpacesFromFrontAndBack(stringValue:String) -> String {
        return stringValue.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    //MARK:- Updating notification label
    
    // Updating notification label Depend on the connections
    func updateNotificationLabelWithConnection(connections:Int)  {
        if connections >= 2 {
            mapRouteButton.isEnabled = true
            if connections == 3 {
                updateNotificationLabel(message: "1 stop")
            } else if connections > 3 {
                updateNotificationLabel(message: "\(connections-2) stops")
            } else if connections == 2 {
                updateNotificationLabel(message: "Direct Flight")
            }
        } else {
            mapRouteButton.isEnabled = false
            updateNotificationLabel(message: "")
        }
    }
    
    func updateNotificationLabel(message:String) {
        notificationLabel.text = message
    }
    
    // MARK: - TextField AutoComplete
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return !autoCompleteText(in : textField, using: string, suggestionsArray: Array(suggestedCountries.keys))
    }
    
    func autoCompleteText(in textField: UITextField, using string: String, suggestionsArray: [String]) -> Bool {
        if !string.isEmpty,
            let selectedTextRange = textField.selectedTextRange,
            selectedTextRange.end == textField.endOfDocument,
            let prefixRange = textField.textRange(from: textField.beginningOfDocument, to: selectedTextRange.start),
            let text = textField.text(in : prefixRange) {
            let prefix = text + string
            var matches = suggestionsArray.filter {
                $0.hasPrefix(prefix)
            }
            
            if matches.count == 0 {
                matches = suggestionsArray.filter {
                    $0.hasPrefix(prefix.uppercased())
                }
            }
            
            if (matches.count > 0) {
                textField.text = matches[0]
                if let start = textField.position(from: textField.beginningOfDocument, offset: prefix.count) {
                    textField.selectedTextRange = textField.textRange(from: start, to: textField.endOfDocument)
                    return true
                }
            }
        }
        
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tripMapViewController" {
            if let tripMapViewController = segue.destination as? TripMapViewController {
                tripMapViewController.connectionArray = connectionArray
            }
        }
    }
}

