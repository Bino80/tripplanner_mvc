//
//  CheapestFlightFinder.swift
//  TripPlanner
//
//  Created by Binoy Jose on 20/07/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import Foundation

class CheapestFlightFinder {
    
    // Searching the cheapest flight cost and retrieve the connections if any
    func findCheapestFlight(edgeType: EdgeType, source: String, dest: String, data: [Response], connectionArray:inout Array<connectionDetails>)
        -> (total: Double, schedule: Schedule) {
            
            var flightGraph = FlightGraphAdjacencyMatrix<FligtConnectionInMemory>()
            
            data.first?.connections.forEach {
                let fromNode = flightGraph.generateFlightGraphVertex(FligtConnectionInMemory($0.from, $0))
                let toNode = flightGraph.generateFlightGraphVertex(FligtConnectionInMemory($0.to, $0, true))
                
                switch edgeType {
                case .directed:
                    // Assigning the price to one direction 'from -> to'
                    flightGraph.addDirectEdge(lhs: fromNode, rhs: toNode, weight: $0.price)
                    
                case .undirected:
                    // Price(weight) available to both direction
                    flightGraph.addDirectEdge(lhs: fromNode, rhs: toNode, weight: $0.price)
                    flightGraph.addDirectEdge(lhs: toNode, rhs: fromNode, weight: $0.price)
                }
            }
            
            let fromNode = flightGraph.generateFlightGraphVertex(FligtConnectionInMemory(source))
            let destNode = flightGraph.generateFlightGraphVertex(FligtConnectionInMemory(dest))
            let searchResult = flightGraph.addtoFlightGraph(source: fromNode)
            let flightVertexArray = searchResult?.searchPathTo(vertex: destNode, graph: flightGraph)
            
            var schedule = Schedule()
            
            var routePoints: [LatLong] = []
            var countries:[String] = []
            var cost: Double = 0
            connectionArray.removeAll()
            
            flightVertexArray?.forEach {
                guard let flight = $0.data.holder else { return }
                cost += flight.price
                routePoints.append(flight.coordinates.from)
                countries.append("\($0.data)")
            }
            
            connectionArray.append(connectionDetails.init(paths: countries, totalAmout: cost))
            
            if  routePoints.count % 2 > 0 {
                routePoints.append((flightVertexArray?.last?.data.holder?.coordinates.from)!)
            }
            
            for idx in stride(from: 0, to: routePoints.count-1, by: 1) {
                let departure = Departure(routePoints[idx])
                let arrival = Departure(routePoints[idx+1])
                let flight = Flight(arrival: arrival, departure: departure)
                schedule.flights?.append(flight)
            }
            
            let total = searchResult?.distance(vertexTo: destNode) ?? 0.0
            
            return (total, schedule)
    }
}

