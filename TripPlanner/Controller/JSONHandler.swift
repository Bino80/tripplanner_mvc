//
//  JSONHandler.swift
//  TripPlanner
//
//  Created by Binoy Jose on 19/07/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import Foundation

class JSONHandler {
    
    // Fetch data from given URL in background
    private  func fetchConnectionsJSONinBackground(completion: @escaping (Response?, Error?) -> ()) {
        let urlString = "https://bitbucket.org/Bino80/json/raw/13071d18af2cbe656ad98819f0857e0f7c797485/connections.json"
        guard let url = URL(string: urlString) else {
            return
        }
        
        DispatchQueue.global(qos: .background).async {
            URLSession.shared.dataTask(with: url) { (data, resp, error) in
                
                if let error = error {
                    completion(nil,error)
                    return
                }
                
                // successful
                do {
                    let connections = try JSONDecoder().decode(Response.self, from: data!)
                    completion(connections, nil)
                    
                } catch let jsonError {
                    completion(nil,jsonError)
                }
            }.resume()
        }
    }
    
    // Adding data from JSON to to suggested Countries Dictionary
    func fetchDataFromJSONinBackground() {
        fetchConnectionsJSONinBackground{ (response, error) in
            if let error = error {
                print("Failed to fetch courses", error)
                return
                
            } else {
                dataFromJSON.append(response!)
                response?.connections.forEach({ (connection) in
                    
                    if suggestedCountries[connection.from] == nil {
                        suggestedCountries.updateValue(GeoCoordinates.init(lat: connection.coordinates.from.lat, long: connection.coordinates.from.long), forKey: connection.from)
                    }
                    
                    if suggestedCountries[connection.to] == nil {
                        suggestedCountries.updateValue(GeoCoordinates.init(lat: connection.coordinates.to.lat, long: connection.coordinates.to.long), forKey: connection.to)
                    }
                })
            }
        }
    }
}
