//
//  SearchShortPath.swift
//  TripPlanner
//
//  Created by Binoy Jose on 20/07/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

public struct SearchShortPath<T: Hashable> {
    var predecessors: [Int?]
    var weights: [Double]
}

extension SearchShortPath {
    func distance(vertexTo: FlightGraphVertex<T>) -> Double? {
        let distance = weights[vertexTo.indexInMatrix]
        guard distance != Double.infinity else {
            return nil
        }
        
        return distance
    }
    
    func searchPathTo(vertex: FlightGraphVertex<T>, graph: FlightGraphAdjacencyMatrix<T>) -> [FlightGraphVertex<T>]? {
        guard weights[vertex.indexInMatrix] != Double.infinity else {
            return nil
        }
        
        guard let predecessorIndex = predecessors[vertex.indexInMatrix] else {
            return nil
        }
        
        let prevVertex = graph.flightGraphVertex[predecessorIndex]
        
        if prevVertex.indexInMatrix == vertex.indexInMatrix {
            return [vertex]
        }
        
        guard let buildPath = searchPathTo(vertex: prevVertex, graph: graph) else {
            return nil
        }
        
        return buildPath + [vertex]
    }
}
