//
//  FlightGraphEdge.swift
//  TripPlanner
//
//  Created by Binoy Jose on 19/07/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import Foundation

public enum EdgeType {
    case directed
    case undirected
}

public struct FlightGraphEdge<T: Hashable>: Equatable {
    var vertexFrom: FlightGraphVertex<T>
    var vartexTo: FlightGraphVertex<T>
    var weight: Double?
}
