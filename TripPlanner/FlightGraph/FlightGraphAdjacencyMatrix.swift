//
//  FlightGraphAdjacencyMatrix.swift
//  TripPlanner
//
//  Created by Binoy Jose on 19/07/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import Foundation

public struct FlightGraphAdjacencyMatrix<T: Hashable>: Equatable {
    typealias ColumnVertices = [Double?]
    var matrix = [ColumnVertices]()
    var flightGraphVertex = [FlightGraphVertex<T>]()
}

extension FlightGraphAdjacencyMatrix {
    var edges: [FlightGraphEdge<T>] {
        var tempEdge = [FlightGraphEdge<T>]()
        for columnIndex in 0..<matrix.count {
            for rowIndex in 0..<matrix.count {
                if let weight = matrix[columnIndex][rowIndex] {
                    tempEdge.append(FlightGraphEdge(vertexFrom: flightGraphVertex[columnIndex],
                                                    vartexTo: flightGraphVertex[rowIndex],
                                                    weight: weight))
                }
            }
        }
        
        return tempEdge
    }
}

extension FlightGraphAdjacencyMatrix {
    mutating func generateFlightGraphVertex(_ newValue: T) -> FlightGraphVertex<T> {
        let matchingVertex = flightGraphVertex.filter { $0.data == newValue }
        
        if matchingVertex.count > 0 {
            return matchingVertex.last!
        }
        
        for columnIndex in 0..<matrix.count {
            matrix[columnIndex].append(nil)
        }
        
        let newVertex = FlightGraphVertex(data: newValue, indexInMatrix: matrix.count)
        let newRow = ColumnVertices(repeating: nil, count: matrix.count + 1)
        flightGraphVertex.append(newVertex)
        matrix.append(newRow)
        
        return newVertex
    }
}

extension FlightGraphAdjacencyMatrix {
    mutating func addDirectEdge(lhs: FlightGraphVertex<T>, rhs: FlightGraphVertex<T>, weight: Double?) {
        matrix[lhs.indexInMatrix][rhs.indexInMatrix] = weight
    }
}

extension FlightGraphAdjacencyMatrix {
    func getWeight(source: FlightGraphVertex<T>, destination: FlightGraphVertex<T>) -> Double? {
        return matrix[source.indexInMatrix][destination.indexInMatrix]
    }
}

extension FlightGraphAdjacencyMatrix {
    func addtoFlightGraph(source: FlightGraphVertex<T>) -> SearchShortPath<T>? {
        var predecessors = [Int?](repeating: nil, count: flightGraphVertex.count)
        var weights = Array(repeating: Double.infinity, count: flightGraphVertex.count)
        
        predecessors[source.indexInMatrix] = source.indexInMatrix
        weights[source.indexInMatrix] = 0
        
        for _ in 0..<flightGraphVertex.count - 1 {
            var weightUpdated = false
            edges.forEach {
                let weight = $0.weight!
                let relaxDistance = weights[$0.vertexFrom.indexInMatrix] + weight
                
                if relaxDistance < weights[$0.vartexTo.indexInMatrix] {
                    predecessors[$0.vartexTo.indexInMatrix] = $0.vertexFrom.indexInMatrix
                    weights[$0.vartexTo.indexInMatrix] = relaxDistance
                    weightUpdated = true
                }
            }
            
            guard weightUpdated else {
                break
            }
        }
        
        for edge in edges where weights[edge.vartexTo.indexInMatrix] > weights[edge.vertexFrom.indexInMatrix] + edge.weight! {
            return nil
        }
        
        return SearchShortPath(predecessors: predecessors, weights: weights)
    }
}
