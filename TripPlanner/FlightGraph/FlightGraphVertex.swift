//
//  FlightGraphVertex.swift
//  TripPlanner
//
//  Created by Binoy Jose on 19/07/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import Foundation

public struct FlightGraphVertex<T: Hashable>: Equatable {
    var data: T
    var indexInMatrix: Int
}
