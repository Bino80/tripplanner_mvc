//
//  FlightGraphTests.swift
//  TripPlannerTests
//
//  Created by Binoy Jose on 20/07/2020.
//  Copyright © 2020 Binoy Jose. All rights reserved.
//

import XCTest
@testable import TripPlanner

class FlightGraphTests: XCTestCase {
    
    var flightGraph:FlightGraphAdjacencyMatrix<String>!
    
    var london:FlightGraphVertex<String>!
    var tokyo:FlightGraphVertex<String>!
    var sydney:FlightGraphVertex<String>!
    var capeTown:FlightGraphVertex<String>!
    var newYork:FlightGraphVertex<String>!
    var losAngeles:FlightGraphVertex<String>!
    var porto:FlightGraphVertex<String>!
    
    override func setUp() {
        super.setUp()
        flightGraph = FlightGraphAdjacencyMatrix<String>()
        london = flightGraph.generateFlightGraphVertex("London")
        tokyo = flightGraph.generateFlightGraphVertex("Tokyo")
        sydney = flightGraph.generateFlightGraphVertex("Sydney")
        capeTown = flightGraph.generateFlightGraphVertex("Cape Town")
        newYork = flightGraph.generateFlightGraphVertex("New York")
        losAngeles = flightGraph.generateFlightGraphVertex("Los Angeles")
        porto = flightGraph.generateFlightGraphVertex("Porto")
        
        flightGraph.addDirectEdge(lhs: london, rhs: porto, weight: 50)
        flightGraph.addDirectEdge(lhs: london, rhs: tokyo, weight: 220)
        flightGraph.addDirectEdge(lhs: tokyo, rhs: london, weight: 200)
        flightGraph.addDirectEdge(lhs: tokyo, rhs: sydney, weight: 100)
        flightGraph.addDirectEdge(lhs: sydney, rhs: capeTown, weight: 200)
        flightGraph.addDirectEdge(lhs: capeTown, rhs: london, weight: 800)
        flightGraph.addDirectEdge(lhs: london, rhs: newYork, weight: 400)
        flightGraph.addDirectEdge(lhs: newYork, rhs: losAngeles, weight: 120)
        flightGraph.addDirectEdge(lhs: losAngeles, rhs: tokyo, weight: 150)
    }
    
    override func tearDown() {
        flightGraph = nil
        london = nil
        tokyo = nil
        sydney = nil
        capeTown = nil
        newYork = nil
        losAngeles = nil
        porto = nil
    }
    
    func testCheapestFlightFromLondonToAllNodes() {
        let  londonNode = flightGraph.addtoFlightGraph(source:london)
        
        let expectedWeight1 = londonNode?.distance(vertexTo:newYork) ?? 0.0
        let expectedResult1 = 400.0
        XCTAssertEqual(expectedWeight1, expectedResult1, "'London' to 'New York' expected result is \(expectedResult1)")
        
        let expectedWeight2 = londonNode?.distance(vertexTo:losAngeles) ?? 0.0
        let expectedResult2 = 520.0
        XCTAssertEqual(expectedWeight2, expectedResult2, "'London' to 'Los Angeles' expected result is \(expectedResult2)")
        
        let expectedWeight3 = londonNode?.distance(vertexTo:tokyo) ?? 0.0
        let expectedResult3 = 220.0
        XCTAssertEqual(expectedWeight3, expectedResult3, "'London' to 'Tokyo' expected result is \(expectedResult3)")
        
        let expectedWeight4 = londonNode?.distance(vertexTo:sydney) ?? 0.0
        let expectedResult4 = 320.0
        XCTAssertEqual(expectedWeight4, expectedResult4, "'London' to 'Sydney' expected result is \(expectedResult4)")
        
        let expectedWeight5 = londonNode?.distance(vertexTo:capeTown) ?? 0.0
        let expectedResult5 = 520.0
        XCTAssertEqual(expectedWeight5, expectedResult5, "'London' to 'Cape Town' expected result is \(expectedResult5)")
        
        let expectedWeight6 = londonNode?.distance(vertexTo:porto) ?? 0.0
        let expectedResult6 = 50.0
        XCTAssertEqual(expectedWeight6, expectedResult6, "'London' to 'Porto' expected result is \(expectedResult6)")
        
        let expectedWeight7 = londonNode?.distance(vertexTo:london) ?? 0.0
        let expectedResult7 = 0.0
        XCTAssertEqual(expectedWeight7, expectedResult7, "'London' to 'London' expected result is \(expectedResult7)")
    }
    
    func testCheapestFlightFromNewYorkToAllNodes() {
        let  newYorkNode = flightGraph.addtoFlightGraph(source:newYork)
        
        let expectedWeight1 = newYorkNode?.distance(vertexTo:newYork) ?? 0.0
        let expectedResult1 = 0.0
        XCTAssertEqual(expectedWeight1, expectedResult1, "'New York' to 'New York' expected result is \(expectedResult1)")
        
        let expectedWeight2 = newYorkNode?.distance(vertexTo:losAngeles) ?? 0.0
        let expectedResult2 = 120.0
        XCTAssertEqual(expectedWeight2, expectedResult2, "'New York' to 'Los Angeles' expected result is \(expectedResult2)")
        
        let expectedWeight3 = newYorkNode?.distance(vertexTo:tokyo) ?? 0.0
        let expectedResult3 = 270.0
        XCTAssertEqual(expectedWeight3, expectedResult3, "'New York' to 'Tokyo' expected result is \(expectedResult3)")
        
        let expectedWeight4 = newYorkNode?.distance(vertexTo:sydney) ?? 0.0
        let expectedResult4 = 370.0
        XCTAssertEqual(expectedWeight4, expectedResult4, "'New York' to 'Sydney' expected result is \(expectedResult4)")
        
        let expectedWeight5 = newYorkNode?.distance(vertexTo:capeTown) ?? 0.0
        let expectedResult5 = 570.0
        XCTAssertEqual(expectedWeight5, expectedResult5, "'New York' to 'Cape Town' expected result is \(expectedResult5)")
        
        let expectedWeight6 = newYorkNode?.distance(vertexTo:porto) ?? 0.0
        let expectedResult6 = 520.0
        XCTAssertEqual(expectedWeight6, expectedResult6, "'New York' to 'Porto' expected result is \(expectedResult6)")
        
        let expectedWeight7 = newYorkNode?.distance(vertexTo:london) ?? 0.0
        let expectedResult7 = 470.0
        XCTAssertEqual(expectedWeight7, expectedResult7, "'New York' to 'London' expected result is \(expectedResult7)")
    }
    
    func testCheapestFlightFromTokyoToAllNodes() {
        let  tokyoNode = flightGraph.addtoFlightGraph(source:tokyo)
        
        let expectedWeight1 = tokyoNode?.distance(vertexTo:newYork) ?? 0.0
        let expectedResult1 = 600.0
        XCTAssertEqual(expectedWeight1, expectedResult1, "'Tokyo' to 'New York' expected result is \(expectedResult1)")
        
        let expectedWeight2 = tokyoNode?.distance(vertexTo:losAngeles) ?? 0.0
        let expectedResult2 = 720.0
        XCTAssertEqual(expectedWeight2, expectedResult2, "'Tokyo' to 'Los Angeles' expected result is \(expectedResult2)")
        
        let expectedWeight3 = tokyoNode?.distance(vertexTo:tokyo) ?? 0.0
        let expectedResult3 = 0.0
        XCTAssertEqual(expectedWeight3, expectedResult3, "'Tokyo' to 'Tokyo' expected result is \(expectedResult3)")
        
        let expectedWeight4 = tokyoNode?.distance(vertexTo:sydney) ?? 0.0
        let expectedResult4 = 100.0
        XCTAssertEqual(expectedWeight4, expectedResult4, "'Tokyo' to 'Sydney' expected result is \(expectedResult4)")
        
        let expectedWeight5 = tokyoNode?.distance(vertexTo:capeTown) ?? 0.0
        let expectedResult5 = 300.0
        XCTAssertEqual(expectedWeight5, expectedResult5, "'Tokyo' to 'Cape Town' expected result is \(expectedResult5)")
        
        let expectedWeight6 = tokyoNode?.distance(vertexTo:porto) ?? 0.0
        let expectedResult6 = 250.0
        XCTAssertEqual(expectedWeight6, expectedResult6, "'Tokyo' to 'Porto' expected result is \(expectedResult6)")
        
        let expectedWeight7 = tokyoNode?.distance(vertexTo:london) ?? 0.0
        let expectedResult7 = 200.0
        XCTAssertEqual(expectedWeight7, expectedResult7, "'Tokyo' to 'London' expected result is \(expectedResult7)")
    }
    
    func testCheapestFlightFromSydneyToAllNodes() {
        let  sydneyNode = flightGraph.addtoFlightGraph(source:sydney)
        
        let expectedWeight1 = sydneyNode?.distance(vertexTo:newYork) ?? 0.0
        let expectedResult1 = 1400.0
        XCTAssertEqual(expectedWeight1, expectedResult1, "'Sydney' to 'New York' expected result is \(expectedResult1)")
        
        let expectedWeight2 = sydneyNode?.distance(vertexTo:losAngeles) ?? 0.0
        let expectedResult2 = 1520.0
        XCTAssertEqual(expectedWeight2, expectedResult2, "'Sydney' to 'Los Angeles' expected result is \(expectedResult2)")
        
        let expectedWeight3 = sydneyNode?.distance(vertexTo:tokyo) ?? 0.0
        let expectedResult3 = 1220.0
        XCTAssertEqual(expectedWeight3, expectedResult3, "'Sydney' to 'Tokyo' expected result is \(expectedResult3)")
        
        let expectedWeight4 = sydneyNode?.distance(vertexTo:sydney) ?? 0.0
        let expectedResult4 = 0.0
        XCTAssertEqual(expectedWeight4, expectedResult4, "'Sydney' to 'Sydney' expected result is \(expectedResult4)")
        
        let expectedWeight5 = sydneyNode?.distance(vertexTo:capeTown) ?? 0.0
        let expectedResult5 = 200.0
        XCTAssertEqual(expectedWeight5, expectedResult5, "'Sydney' to 'Cape Town' expected result is \(expectedResult5)")
        
        let expectedWeight6 = sydneyNode?.distance(vertexTo:porto) ?? 0.0
        let expectedResult6 = 1050.0
        XCTAssertEqual(expectedWeight6, expectedResult6, "'Sydney' to 'Porto' expected result is \(expectedResult6)")
        
        let expectedWeight7 = sydneyNode?.distance(vertexTo:london) ?? 0.0
        let expectedResult7 = 1000.0
        XCTAssertEqual(expectedWeight7, expectedResult7, "'Sydney' to 'London' expected result is \(expectedResult7)")
    }
    
    func testCheapestFlightFromCapeTownToAllNodes() {
        let  capeTownNode = flightGraph.addtoFlightGraph(source:capeTown)
        
        let expectedWeight1 = capeTownNode?.distance(vertexTo:newYork) ?? 0.0
        let expectedResult1 = 1200.0
        XCTAssertEqual(expectedWeight1, expectedResult1, "'Cape Town' to 'New York' expected result is \(expectedResult1)")
        
        let expectedWeight2 = capeTownNode?.distance(vertexTo:losAngeles) ?? 0.0
        let expectedResult2 = 1320.0
        XCTAssertEqual(expectedWeight2, expectedResult2, "'Cape Town' to 'Los Angeles' expected result is \(expectedResult2)")
        
        let expectedWeight3 = capeTownNode?.distance(vertexTo:tokyo) ?? 0.0
        let expectedResult3 = 1020.0
        XCTAssertEqual(expectedWeight3, expectedResult3, "'Cape Town' to 'Tokyo' expected result is \(expectedResult3)")
        
        let expectedWeight4 = capeTownNode?.distance(vertexTo:sydney) ?? 0.0
        let expectedResult4 = 1120.0
        XCTAssertEqual(expectedWeight4, expectedResult4, "'Cape Town' to 'Sydney' expected result is \(expectedResult4)")
        
        let expectedWeight5 = capeTownNode?.distance(vertexTo:capeTown) ?? 0.0
        let expectedResult5 = 0.0
        XCTAssertEqual(expectedWeight5, expectedResult5, "'Cape Town' to 'Cape Town' expected result is \(expectedResult5)")
        
        let expectedWeight6 = capeTownNode?.distance(vertexTo:porto) ?? 0.0
        let expectedResult6 = 850.0
        XCTAssertEqual(expectedWeight6, expectedResult6, "'Cape Town' to 'Porto' expected result is \(expectedResult6)")
        
        let expectedWeight7 = capeTownNode?.distance(vertexTo:london) ?? 0.0
        let expectedResult7 = 800.0
        XCTAssertEqual(expectedWeight7, expectedResult7, "'Cape Town' to 'London' expected result is \(expectedResult7)")
    }
    
    func testCheapestFlightFromLosAngelesToAllNodes() {
        let  losAngelesNode = flightGraph.addtoFlightGraph(source:losAngeles)
        
        let expectedWeight1 = losAngelesNode?.distance(vertexTo:newYork) ?? 0.0
        let expectedResult1 = 750.0
        XCTAssertEqual(expectedWeight1, expectedResult1, "'Los Angeles' to 'New York' expected result is \(expectedResult1)")
        
        let expectedWeight2 = losAngelesNode?.distance(vertexTo:losAngeles) ?? 0.0
        let expectedResult2 = 0.0
        XCTAssertEqual(expectedWeight2, expectedResult2, "'Los Angeles' to 'Los Angeles' expected result is \(expectedResult2)")
        
        let expectedWeight3 = losAngelesNode?.distance(vertexTo:tokyo) ?? 0.0
        let expectedResult3 = 150.0
        XCTAssertEqual(expectedWeight3, expectedResult3, "'Los Angeles' to 'Tokyo' expected result is \(expectedResult3)")
        
        let expectedWeight4 = losAngelesNode?.distance(vertexTo:sydney) ?? 0.0
        let expectedResult4 = 250.0
        XCTAssertEqual(expectedWeight4, expectedResult4, "'Los Angeles' to 'Sydney' expected result is \(expectedResult4)")
        
        let expectedWeight5 = losAngelesNode?.distance(vertexTo:capeTown) ?? 0.0
        let expectedResult5 = 450.0
        XCTAssertEqual(expectedWeight5, expectedResult5, "'Los Angeles' to 'Cape Town' expected result is \(expectedResult5)")
        
        let expectedWeight6 = losAngelesNode?.distance(vertexTo:porto) ?? 0.0
        let expectedResult6 = 400.0
        XCTAssertEqual(expectedWeight6, expectedResult6, "'Los Angeles' to 'Porto' expected result is \(expectedResult6)")
        
        let expectedWeight7 = losAngelesNode?.distance(vertexTo:london) ?? 0.0
        let expectedResult7 = 350.0
        XCTAssertEqual(expectedWeight7, expectedResult7, "'Los Angeles' to 'London' expected result is \(expectedResult7)")
    }
}
