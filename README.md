# README #

## This is one of my exercises,developed to refresh my algorithm skills using Swift ##

App started by making a request to: https://bitbucket.org/Bino80/json/raw/13071d18af2cbe656ad98819f0857e0f7c797485/connections.json and parsing  the JSON

This App is to help the user to find the cheaper flight between any cities, whether directly and indirectly connected.
It will also recalculate or update the cost label if the user changes any of the selected cities.

This App will calculate the cheapest route and show it in the label when departure and destination are selected. 

I have also added autocompletion in the text fields to fill the name of departure and destination city if city names are in JSON . 
This App also shows the selected route on the map.

I have covered all the possible search algorithm logic in the unit test.


### How do I get set up? ###

* This project is build in Xcode_11.4.1  and Swift 5+
